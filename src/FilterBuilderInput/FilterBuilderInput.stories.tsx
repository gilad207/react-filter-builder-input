import React, { useState } from "react";
import FilterBuilderInput from "./FilterBuilderInput";
import { LabelProps, LabelType } from "../PropTypes";
import styled from "styled-components";

export default {
  title: "FilterBuilderInput",
};

export const SimpleFilterWithNoOptions = () => {
  const [value, setValue] = useState();

  return <FilterBuilderInput filter={value} onChange={setValue} />;
};

export const SimpleFilterWithOptions = () => {
  const [value, setValue] = useState();
  const [fieldSelected, setFieldSelected] = useState<string | undefined>();

  const valuesByField = {
    phone: ["201-241-2412", "087-211-2412", "201-111-4362", "214-241-4166"],
    address: ["bannana 24", "shishim 10", "some address", "olive 214"],
    hight: ["241", "12", "3.21", "251"],
  };

  function changeFieldSelected(inputValue, field, relation, values) {
    setFieldSelected(field);
  }

  return (
    <FilterBuilderInput
      filter={value}
      onChange={setValue}
      fieldOptions={["phone", "address", "hight", "capacity", "order number"]}
      valueOptions={valuesByField[fieldSelected] || []}
      onOptionsContextChange={changeFieldSelected}
    />
  );
};

function CustomLabel({ color, label, labelType, onClick }: LabelProps) {
  if (labelType === LabelType.BooleanRelation) {
    return <ColoredContainer onClick={onClick}>{label}</ColoredContainer>;
  }

  return <StyledLabelContainer onClick={onClick}>{label}</StyledLabelContainer>;
}

const StyledLabelContainer = styled.div`
  padding: 0.2rem;
  display: flex;
  width: fit-content;
`;

const ColoredContainer = styled.div`
  padding: 0.2rem;
  display: flex;
  width: fit-content;
  color: blue;
`;

export const FilterWithCustomLabels = () => {
  const [value, setValue] = useState();
  const [fieldSelected, setFieldSelected] = useState<string | undefined>();

  const valuesByField = {
    phone: ["201-241-2412", "087-211-2412", "201-111-4362", "214-241-4166"],
    address: ["bannana 24", "shishim 10", "some address", "olive 214"],
    hight: ["241", "12", "3.21", "251"],
  };

  function changeFieldSelected(inputValue, field, relation, values) {
    setFieldSelected(field);
  }

  return (
    <FilterBuilderInput
      filter={value}
      onChange={setValue}
      fieldOptions={["phone", "address", "hight", "capacity", "order number"]}
      valueOptions={valuesByField[fieldSelected] || []}
      onOptionsContextChange={changeFieldSelected}
      LabelComponent={CustomLabel}
    />
  );
};

export const FilterWithCustomRelations = () => {
  const [value, setValue] = useState();
  const [fieldSelected, setFieldSelected] = useState<string | undefined>();

  const valuesByField = {
    phone: ["201-241-2412", "087-211-2412", "201-111-4362", "214-241-4166"],
    address: ["bannana 24", "shishim 10", "some address", "olive 214"],
    hight: ["241", "12", "3.21", "251"],
  };

  function changeFieldSelected(inputValue, field, relation, values) {
    setFieldSelected(field);
  }

  return (
    <FilterBuilderInput
      filter={value}
      onChange={setValue}
      fieldOptions={["phone", "address", "hight", "capacity", "order number"]}
      valueOptions={valuesByField[fieldSelected] || []}
      onOptionsContextChange={changeFieldSelected}
      relationOptions={["Crazy", "Not Crazy", "A little crazy"]}
    />
  );
};

export const FilterWithCustomInputStyles = () => {
  const [value, setValue] = useState();
  const [fieldSelected, setFieldSelected] = useState<string | undefined>();

  const valuesByField = {
    phone: ["201-241-2412", "087-211-2412", "201-111-4362", "214-241-4166"],
    address: ["bannana 24", "shishim 10", "some address", "olive 214"],
    hight: ["241", "12", "3.21", "251"],
  };

  function changeFieldSelected(inputValue, field, relation, values) {
    setFieldSelected(field);
  }

  return (
    <FilterBuilderInput
      filter={value}
      onChange={setValue}
      fieldOptions={["phone", "address", "hight", "capacity", "order number"]}
      valueOptions={valuesByField[fieldSelected] || []}
      onOptionsContextChange={changeFieldSelected}
      inputStyle={{ fontFamily: "Consolas" }}
    />
  );
};
