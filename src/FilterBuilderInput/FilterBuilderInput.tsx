import React, { useEffect } from "react";
import styled from "styled-components";

import { FilterBuilderInputProps } from "./FilterBuilderInput.types";
import LogicalRelationBuilder from "../LogicalRelationBuilder/LogicalRelationBuilder";
import { BooleanRelationOptions, newEmptyLogicalRelation } from "../DataTypes";
import LabelChip from "../LabelChip/LabelChip";

const StyledDiv = styled.div`
  display: flex;
  flex: 1;
  border-radius: 0.4rem;
  border-style: solid;
  border-width: 0.1rem;
  border-color: rgba(0, 0, 0, 0.4);
  padding: 0.2rem;
  flex-direction: row;
  flex-wrap: wrap;
`;

const FilterBuilderInput: React.FC<FilterBuilderInputProps> = ({
  filter,
  onChange,
  colorPalette = defaultColorPalette,
  fieldOptions,
  isLoadingFieldOptions,
  valueOptions,
  isLoadingValueOptions,
  onOptionsContextChange,
  loadingIndicator,
  LabelComponent = LabelChip,
  relationOptions = BooleanRelationOptions,
  inputStyle,
}) => {
  useEffect(() => {
    if (!filter) {
      onChange(newEmptyLogicalRelation());
    }
  }, [filter]);

  return (
    <StyledDiv data-testid="test-component">
      {filter && (
        <LogicalRelationBuilder
          onChange={onChange}
          value={filter}
          isComplete={false}
          colorPalette={colorPalette}
          fieldOptions={fieldOptions}
          isLoadingFieldOptions={isLoadingFieldOptions}
          valueOptions={valueOptions}
          isLoadingValueOptions={isLoadingValueOptions}
          onOptionsContextChange={onOptionsContextChange}
          loadingIndicator={loadingIndicator}
          LabelComponent={LabelComponent}
          relationOptions={relationOptions}
          inputStyle={inputStyle}
        />
      )}
    </StyledDiv>
  );
};

const defaultColorPalette = [
  "rgba(0,0,0,0.1)",
  "#64b5f6",
  "#7986cb",
  "#9575cd",
  "#4db6ac",
  "#4dd0e1",
];

export default FilterBuilderInput;
