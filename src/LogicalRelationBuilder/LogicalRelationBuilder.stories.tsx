import React, { useState } from "react";
import LogicalRelationBuilder from "./LogicalRelationBuilder";
import { newEmptyLogicalRelation } from "../DataTypes";

export default {
  title: "LogicalRelationBuilder",
};

export const SimpleLogicalRelationBuilder = () => {
  const [value, setValue] = useState(newEmptyLogicalRelation());

  return (
    <LogicalRelationBuilder
      value={value}
      onChange={setValue}
      isComplete={false}
      colorPalette={[
        "rgba(0,0,0,0.1)",
        "#64b5f6",
        "#7986cb",
        "#9575cd",
        "#4db6ac",
        "#4dd0e1",
      ]}
    />
  );
};
