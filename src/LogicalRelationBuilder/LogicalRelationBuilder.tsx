import React, { useEffect, useRef, useState } from "react";
import {
  BooleanRelationOptions,
  FieldMatcher,
  FilterElement,
  FilterElementType,
  LogicalRelation,
  LogicalRelationType,
  newEmptyFieldMatcher,
  newEmptyLogicalRelation,
} from "../DataTypes";
import styled from "styled-components";
import FieldMatcherBuilder from "../FieldMatcherBuilder/FieldMatcherBuilder";
import LabelChip from "../LabelChip/LabelChip";
import FilterInput from "../FilterInput";
import { LabelComponentType, LabelType, OptionsProps } from "../PropTypes";

export type Props = {
  onChange: (LogicalRelation) => any;
  value: LogicalRelation;
  onSelfDelete?: () => any;
  isComplete: boolean;
  onComplete?: () => any;
  depth?: number;
  colorPalette: string[];
  LabelComponent?: LabelComponentType;
} & OptionsProps;

function pushBetween<T>(elements: T[], elementToPush: (number) => T): T[] {
  const newArray = [];

  for (const index in elements) {
    newArray.push(elements[index]);

    if (Number(index) < elements.length - 1) {
      newArray.push(elementToPush(index));
    }
  }

  return newArray;
}

type ElementUpdateFunction = (elems: FilterElement[]) => FilterElement[];

function getLevelColor(depth: number, pallete: string[]) {
  return depth >= 0 ? pallete[depth % pallete.length] : undefined;
}

export default function LogicalRelationBuilder({
  value,
  onChange,
  onSelfDelete,
  isComplete,
  onComplete,
  depth = 0,
  colorPalette,
  fieldOptions,
  isLoadingFieldOptions,
  valueOptions,
  isLoadingValueOptions,
  onOptionsContextChange,
  loadingIndicator,
  LabelComponent = LabelChip,
  relationOptions = BooleanRelationOptions,
  inputStyle,
}: Props) {
  const [fieldMatcher, setFieldMatcher] = useState(newEmptyFieldMatcher());
  const [logicalRelation, setLogicalRelation] = useState(
    newEmptyLogicalRelation()
  );
  const [isLogicalRelationComplete, setIsLogicalRelationComplete] = useState(
    false
  );
  const [typeInBuild, setTypeInBuild] = useState<string | undefined>();
  const [inputValue, setInputValue] = useState("");

  const currentLevelColor = getLevelColor(depth, colorPalette);
  const nextLevelColor = getLevelColor(depth + 1, colorPalette);

  function setElements(updateFunction: ElementUpdateFunction) {
    onChange({
      ...value,
      elements: updateFunction(value.elements),
    });
  }

  function toggleRelation() {
    onChange({
      ...value,
      relation:
        value.relation === LogicalRelationType.And
          ? LogicalRelationType.Or
          : LogicalRelationType.And,
    });
  }

  function cancelTypeInBuild() {
    setTypeInBuild(undefined);
    setFieldMatcher(newEmptyFieldMatcher());
    setLogicalRelation(newEmptyLogicalRelation());
  }

  function appendElement(element: FilterElement) {
    setElements((elems) => [...elems, { ...element }]);
  }

  function replaceElement(element: FilterElement, index: number) {
    setElements((elems) => {
      const copy = [...elems];
      copy.splice(index, 1, element);

      return copy;
    });
  }

  function removeLast() {
    if (value.elements.length === 0) {
      onSelfDelete && onSelfDelete();
    }

    setElements((elems) => {
      const copy = [...elems];
      copy.splice(elems.length - 1, 1);

      return copy;
    });
  }

  function buildByInputValue(newValue) {
    if (newValue != "(" && newValue != ")") {
      setInputValue(newValue);
      setTypeInBuild(FilterElementType.FieldMatcher);
    }

    if (newValue === "(") {
      setTypeInBuild(FilterElementType.LogicalRelation);
    }

    if (newValue === ")") {
      onComplete && onComplete();
    }
  }

  useEffect(() => {
    if (typeInBuild && inputValue) {
      setInputValue("");
    }
  }, [typeInBuild, inputValue]);

  useEffect(() => {
    if (fieldMatcher.field && fieldMatcher.relation && fieldMatcher.value) {
      appendElement(fieldMatcher);
      setFieldMatcher(newEmptyFieldMatcher());
      setTypeInBuild(undefined);
    }
  }, [fieldMatcher]);

  useEffect(() => {
    if (isLogicalRelationComplete) {
      if (logicalRelation.elements.length) {
        appendElement(logicalRelation);
      }

      setTypeInBuild(undefined);
      setLogicalRelation(newEmptyLogicalRelation());
      setIsLogicalRelationComplete(false);
    }
  }, [isLogicalRelationComplete, logicalRelation]);

  return (
    <BuilderContainer>
      {pushBetween(
        value.elements.map((element, index) =>
          element.type == FilterElementType.FieldMatcher ? (
            <FieldMatcherBuilder
              key={"element_in_relation" + index}
              chipsColor={currentLevelColor}
              onChange={(updatedElement) =>
                replaceElement(updatedElement, index)
              }
              value={element as FieldMatcher}
              fieldOptions={fieldOptions}
              isLoadingFieldOptions={isLoadingFieldOptions}
              valueOptions={valueOptions}
              isLoadingValueOptions={isLoadingValueOptions}
              onOptionsContextChange={onOptionsContextChange}
              loadingIndicator={loadingIndicator}
              LabelComponent={LabelComponent}
              relationOptions={relationOptions}
              inputStyle={inputStyle}
            />
          ) : (
            [
              <LabelComponent
                label={"("}
                key={"open_bracket" + index}
                color={nextLevelColor}
                labelType={LabelType.Brackets}
              />,
              <LogicalRelationBuilder
                key={"element_in_relation" + index}
                onChange={(updatedElement) =>
                  replaceElement(updatedElement, index)
                }
                value={element as LogicalRelation}
                isComplete={true}
                depth={depth + 1}
                colorPalette={colorPalette}
                fieldOptions={fieldOptions}
                isLoadingFieldOptions={isLoadingFieldOptions}
                valueOptions={valueOptions}
                isLoadingValueOptions={isLoadingValueOptions}
                onOptionsContextChange={onOptionsContextChange}
                loadingIndicator={loadingIndicator}
                LabelComponent={LabelComponent}
                relationOptions={relationOptions}
                inputStyle={inputStyle}
              />,
              <LabelComponent
                label={")"}
                key={"close_bracket" + index}
                color={nextLevelColor}
                labelType={LabelType.Brackets}
              />,
            ]
          )
        ),
        (index) => (
          <LabelComponent
            label={value.relation}
            key={"label_of_relation" + index}
            onClick={toggleRelation}
            color={currentLevelColor}
            labelType={LabelType.LogicalRelation}
          />
        )
      )}
      {!isComplete && typeInBuild === FilterElementType.FieldMatcher && (
        <>
          {value.elements.length > 0 && (
            <LabelComponent
              label={value.relation}
              color={currentLevelColor}
              labelType={LabelType.LogicalRelation}
            />
          )}
          <FieldMatcherBuilder
            chipsColor={currentLevelColor}
            value={fieldMatcher}
            initialInputValue={inputValue}
            onChange={setFieldMatcher}
            onSelfDelete={cancelTypeInBuild}
            fieldOptions={fieldOptions}
            isLoadingFieldOptions={isLoadingFieldOptions}
            valueOptions={valueOptions}
            isLoadingValueOptions={isLoadingValueOptions}
            onOptionsContextChange={onOptionsContextChange}
            loadingIndicator={loadingIndicator}
            LabelComponent={LabelComponent}
            relationOptions={relationOptions}
            inputStyle={inputStyle}
          />
        </>
      )}
      {!isComplete && typeInBuild === FilterElementType.LogicalRelation && (
        <>
          {value.elements.length > 0 && (
            <LabelComponent
              label={value.relation}
              color={currentLevelColor}
              labelType={LabelType.LogicalRelation}
            />
          )}
          <LabelComponent
            label={"("}
            color={nextLevelColor}
            labelType={LabelType.Brackets}
          />
          <LogicalRelationBuilder
            value={logicalRelation}
            onChange={setLogicalRelation}
            onSelfDelete={cancelTypeInBuild}
            onComplete={() => setIsLogicalRelationComplete(true)}
            isComplete={isLogicalRelationComplete}
            depth={depth + 1}
            colorPalette={colorPalette}
            fieldOptions={fieldOptions}
            isLoadingFieldOptions={isLoadingFieldOptions}
            valueOptions={valueOptions}
            isLoadingValueOptions={isLoadingValueOptions}
            onOptionsContextChange={onOptionsContextChange}
            loadingIndicator={loadingIndicator}
            LabelComponent={LabelComponent}
            relationOptions={relationOptions}
            inputStyle={inputStyle}
          />
        </>
      )}
      {!isComplete && typeInBuild === undefined && (
        <FilterInput
          value={inputValue}
          setValue={buildByInputValue}
          onBackspace={removeLast}
          onClick={() => buildByInputValue("")}
          onArrowDown={() => buildByInputValue("")}
          inputStyle={inputStyle}
        />
      )}
    </BuilderContainer>
  );
}

const BuilderContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: fit-content;
  flex-direction: row;
`;
