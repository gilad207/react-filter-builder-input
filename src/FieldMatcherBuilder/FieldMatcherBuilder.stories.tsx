import React, { useEffect, useState } from "react";
import FieldMatcherBuilder from "./FieldMatcherBuilder";
import { FieldMatcher, newEmptyFieldMatcher } from "../DataTypes";

export default {
  title: "FieldMatcherBuilder",
};

export const MatcherBuilder = () => {
  const [value, setValue] = useState<FieldMatcher>(newEmptyFieldMatcher());

  return <FieldMatcherBuilder value={value} onChange={setValue} />;
};

const fields = ["phone number", "address", "name", "id"];
const valuesOfPhone = [
  "02414421421",
  "4214142151",
  "634634643634",
  "532526326",
];
const valuesOfAdress = ["aba 53", "sagmwa 352", "arnpaw 21"];

const filterArray = (arr, value) =>
  arr.filter((v) => v.toLowerCase().includes(value.toLowerCase()));

function fetchFields(): Promise<string[]> {
  return new Promise((r) => {
    setTimeout(() => {
      r(fields);
    }, 1000);
  });
}

function fetchValues(field): Promise<string[]> {
  return new Promise((r) => {
    setTimeout(() => {
      if (field === "address") {
        r(valuesOfAdress);
      } else if (field === "phone number") {
        r(valuesOfPhone);
      } else {
        r([]);
      }
    }, 3000);
  });
}

export const MatcherBuilderWithPromises = () => {
  const [value, setValue] = useState<FieldMatcher>(newEmptyFieldMatcher());
  const [isLoadingValues, setIsLoadingValues] = useState(false);
  const [isLoadingFields, setIsLoadingFields] = useState(false);
  const [fields, setFields] = useState([]);
  const [filteredFields, setFilteredFields] = useState([]);
  const [values, setValues] = useState([]);
  const [filteredValues, setFilteredValues] = useState([]);

  const [innerInputValue, setInnerInputValue] = useState("");
  const [currentFieldSelected, setCurrentFieldSelected] = useState<
    string | undefined
  >();

  function updateLocalState(inputValue, field, relation, value) {
    setInnerInputValue(inputValue);
    setCurrentFieldSelected(field);
  }

  useEffect(() => {
    setIsLoadingFields(true);

    fetchFields()
      .then(setFields)
      .finally(() => setIsLoadingFields(false));
  }, []);

  useEffect(() => {
    setIsLoadingValues(true);

    fetchValues(currentFieldSelected)
      .then(setValues)
      .finally(() => setIsLoadingValues(false));
  }, [currentFieldSelected]);

  useEffect(() => {
    setFilteredFields(
      fields.includes(innerInputValue)
        ? fields
        : filterArray(fields, innerInputValue)
    );
  }, [fields, innerInputValue]);

  useEffect(() => {
    setFilteredValues(
      values.includes(innerInputValue)
        ? values
        : filterArray(values, innerInputValue)
    );
  }, [values, innerInputValue]);

  return (
    <FieldMatcherBuilder
      value={value}
      onChange={setValue}
      fieldOptions={filteredFields}
      isLoadingFieldOptions={isLoadingFields}
      valueOptions={filteredValues}
      isLoadingValueOptions={isLoadingValues}
      onOptionsContextChange={updateLocalState}
      loadingIndicator={<div>this takes too much time</div>}
    />
  );
};

export const MatcherBuilderWithLongListOfOptions = () => {
  const [value, setValue] = useState<FieldMatcher>(newEmptyFieldMatcher());

  return (
    <FieldMatcherBuilder
      value={value}
      onChange={setValue}
      fieldOptions={[
        "phone number",
        "address",
        "name",
        "id",
        "phone number",
        "address",
        "name",
        "id",
        "phone number",
        "address",
        "name",
        "id",
        "phone number",
        "address",
        "name",
        "id",
        "phone number",
        "address",
        "name",
        "id",
        "phone number",
        "address",
        "name",
        "id",
        "phone number",
        "address",
        "name",
        "id",
      ]}
    />
  );
};
