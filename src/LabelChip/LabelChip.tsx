import React from "react";
import styled from "styled-components";
import { LabelProps } from "../PropTypes";

export default function LabelChip({ label, color, onClick }: LabelProps) {
  return (
    <ChipContainer color={color} onClick={onClick}>
      {label}
    </ChipContainer>
  );
}

const ChipContainer = styled.div`
  display: flex;
  width: fit-content;
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
    "Noto Sans", Ubuntu, Cantarell, "Helvetica Neue", sans-serif,
    "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
  font-size: 1.1rem;
  padding: 0.5rem;
  background-color: ${(props) =>
    props.color ? props.color : `rgba(0,0,0,0.1)`};
  border-radius: 0.4rem;
  cursor: ${(props) => (props.onClick ? "pointer" : "default")};
  margin: 0.1rem;

  &:hover {
    opacity: ${(props) => (props.onClick ? 0.8 : 1.0)};
  }
`;
