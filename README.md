<h1 align="center">Welcome to react-filter-builder-input 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.1-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Filter Builder Input component inspired by Gitlab

Let your users build complex filters fast and easly!

### 🏠 [Homepage](https://gitlab.com/obenyaish/react-filter-builder-input)

### ✨ [Demo](https://codesandbox.io/s/simple-example-8q2w2?file=/src/App.js)

### ✨✨ [Advanced Usage Demo](https://codesandbox.io/s/advanced-usage-demo-drdvq?file=/src/App.js)
## Install

```sh
npm i react-filter-builder-input
```

## Usage

```typescript jsx
const SimpleFilterWithOptions = () => {
  const [filter, setFilter] = useState();
  const [fieldSelected, setFieldSelected] = useState<string | undefined>();

  const valuesByField = {
    phone: ["201-241-2412", "087-211-2412", "201-111-4362", "214-241-4166"],
    address: ["bannana 24", "shishim 10", "some address", "olive 214"],
    hight: ["241", "12", "3.21", "251"],
  };

  function changeFieldSelected(inputValue, field, relation, value) {
    setFieldSelected(field);
  }

  return (
    <FilterBuilderInput
      filter={filter}
      onChange={setFilter}
      fieldOptions={["phone", "address", "hight", "capacity", "order number"]}
      valueOptions={valuesByField[fieldSelected] || []}
      onOptionsContextChange={changeFieldSelected}
    />
  );
};
```

![Example Result](https://gitlab.com/obenyaish/react-filter-builder-input/-/raw/master/demo/filter-builder-example.gif)

Resolved filter state

```json
{
  "type": "LogicalRelation",
  "relation": "Or",
  "elements": [
    {
      "type": "FieldMatcher",
      "value": "087-211-2412",
      "field": "phone",
      "relation": "!="
    },
    {
      "type": "FieldMatcher",
      "value": "32",
      "field": "phone",
      "relation": "Contains"
    },
    {
      "type": "LogicalRelation",
      "relation": "And",
      "elements": [
        {
          "type": "FieldMatcher",
          "value": "30",
          "field": "capacity",
          "relation": ">"
        },
        {
          "type": "FieldMatcher",
          "value": "54",
          "field": "capacity",
          "relation": "<"
        },
        {
          "type": "LogicalRelation",
          "relation": "And",
          "elements": [
            {
              "type": "FieldMatcher",
              "value": "10",
              "field": "hight",
              "relation": "<"
            },
            {
              "type": "FieldMatcher",
              "value": "30",
              "field": "hight",
              "relation": ">"
            }
          ]
        }
      ]
    }
  ]
}
```

## Props

| Property                | Type                                       | Default Value                                                              | Description                                                                                                                                                                                                                                                                                                                            | Notes                                                                                                                                                                                                                                                  |
| ----------------------- | ------------------------------------------ | -------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| filter                  | `LogicalRelation`                          |                                                                            | This component is controlled. Therefore the component will display only the values passed to filter.                                                                                                                                                                                                                                   | Safe to pass undefined. Will invoke onChange with empty filter.                                                                                                                                                                                        |
| onChange                | `(LogicalRelation) => any`                 |                                                                            | This component is controlled. Therefore any change in value will invoke onChange. The change only takes place if filter is updated.                                                                                                                                                                                                    | Must provide onChange.                                                                                                                                                                                                                                 |
| fieldOptions?           | `string[]`                                 |                                                                            | The options that will pop when the field input is focused.                                                                                                                                                                                                                                                                             | See auto complete example in [advanced usage demo](https://codesandbox.io/s/advanced-usage-demo-drdvq?file=/src/App.js)                                                                                                                                                                                                                                                      |
| isLoadingFieldOptions?  | `boolean`                                  |                                                                            | If true the loading indicator will show and no field options will appear.                                                                                                                                                                                                                                                              |                                                                                                                                                                                                                                                        |
| valueOptions?           | `string[]`                                 |                                                                            | The options that will pop when the value input is focused.                                                                                                                                                                                                                                                                             | See auto complete example in [advanced usage demo](https://codesandbox.io/s/advanced-usage-demo-drdvq?file=/src/App.js)                                                                                                                                                                                    |
| isLoadingValueOptions?  | `boolean`                                  |                                                                            | If true the loading indicator will show and not value options will appear.                                                                                                                                                                                                                                                             |                                                                                                                                                                                                                                                        |
| onOptionsContextChange? | `OnOptionsContextChange`                   |                                                                            | A function used to listen for the context of the currently focused input. inputValue is the currently incomplete value in the input. field is the currently selected field (phone, address, ...). relation is the currently selected relation (>, !=, =, ...). value is the currently selected value (104-212-0421, 054-214-5211, ...) | This function should only be used to update a state. And cannot use any other variable. Only the first function passed when the FilterBuilderInput mounts, will be used! onOptionsContextChange cannot be changed after FilterBuilderInput is mounted! |
| loadingIndicator?       | `React.ReactNode` or `null` or `undefined` | styled div with the text: "Loading..."                                     | Render here the indicator you want to appear inside the pop over when loading.                                                                                                                                                                                                                                                         |                                                                                                                                                                                                                                                        |
| colorPalette?           | `string[]`                                 | `["rgba(0,0,0,0.1)", "#64b5f6", "#7986cb", "#9575cd", "#4db6ac", "#4dd0e1"]` | colorPalette to use when a new depth is created. The first color is given to the first (most outer) logical relation. The second to the second, etc...                                                                                                                                                                                 |                                                                                                                                                                                                                                                        |
| LabelComponent? | `LabelComponentType` | `ChipLabel` | This component can be used to override the default chip component | See [advanced usage demo](https://codesandbox.io/s/advanced-usage-demo-drdvq?file=/src/App.js) for more details
| relationOptions? | `string[]` | `["=", "!=", ">", ">=", "<", "<=", "Contains", "No Contains", "Like"]` | Can be used to specify the relatons available. | There is already auto complete on the relation dropdown (no need to implement it). This options doesn't support loading.
| inputStyle? | `React.CSSProperties` | | Can be used to override style of input line. Recommended for changing font related styles. | The style will apply to both the input and the options list text containers. 

## Author

👤 **Ofek Ben-Yaish**

- Github: [@ofekby](https://github.com/ofekby)
- LinkedIn: [Ofek Ben-Yaish](https://linkedin.com/in/ofek-ben-yaish-52a6b718b)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/obenyaish/react-filter-builder-input/-/issues).

## Show your support

Give a ⭐️ if this project helped you!

## Thanks

   * [@HarveyD](https://github.com/HarveyD) for his [template repository](https://github.com/HarveyD/react-component-library/tree/styled-components)

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
